<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="img/Corporate-Governance-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>SHAREHOLDERS INFORMATION</h2>
						<p>INVESTORS</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
		
	</section>
	<!-- Cta Section End -->

	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row justify-content-center">  
				<button class="button active" onClick="overviewFunction(this)"><a>Overview</a></button>
				<button class="button" onClick="auditFunction(this)"><a>Primary and Controlling Shareholders</a></button> 
			</div>
		</div>
	</section>
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
			</br>
			<div class="about-text">
				<div class="section-title"id="Overview"> 
					<div class="container">
			            <div class="table-responsive">
			                <table class="table table-bordered">
			                    <caption>List of Shareholders above 5% as of December 31, 2019.</caption>
			                    <thead> 
			                        <tr>
			                            <th scope="col" style="text-align: center;">No.</th>
			                            <th scope="col" style="text-align: center;">Shareholders Name</th>
			                            <th scope="col" style="text-align: center;">Number of Share</th>
			                            <th scope="col" style="text-align: center;">%</th>
			                        </tr> 
			                    </thead>
			                    <tbody>
			                        <tr>  
			                          	<th scope="row" style="text-align: center;">1</th>
			                          	<td>Dato' Dr. Low Tuck Kwong</td>
			                          	<td>1,798,799,900</td>
			                          	<td>53.96</td>
			                        </tr>
			                        <tr>
			                          	<th scope="row" style="text-align: center;">2</th>
			                          	<td>PT. Sumber Suryadaya Prima</td>
			                          	<td>333,338,000</td>
			                          	<td>10.00</td>
			                        </tr>
			                        <tr>
			                          	<th scope="row" style="text-align: center;">3</th>
			                          	<td>Engki Wibowo</td>
			                          	<td>198,707,500</td>
			                          	<td>5.96</td>
			                        </tr>
			                        <tr>
				                        <th scope="row" style="text-align: center;">4</th>
				                        <td>Public</td>
				                        <td>1,002,488,100</td>
				                        <td>30.08</td>
			                        </tr>
			                        <tr>
			                        	<th scope="col" colspan="2" style="text-align: center;">Total</th>
			                        	<th>3,333,333,500</th>
			                          	<th>100</th>
			                        </tr>
			                    </tbody>
			                </table>
			            </div>
			        </div>
			    </div>
				</div>	
			</div>
			<div class="row">
				<div class="about-text">
				<div class="section-title"id="Audit" style="display: none;">
					<p>As at 30 November 2020, the direct primary and controlling shareholders is Dato’ Dr. Low Tuck Kwong with ownership of 53.96% of total shares with valid voting rights issued by PT Tri Oetama Persada. Below is the diagram of the Company's shareholders:   </p>
		        	<img src="assets/img/Corporate Structure/organization-Structure.png" class="center">  
				</div>	
			</div> 
		</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>