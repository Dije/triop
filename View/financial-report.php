<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
  <!-- Page Preloder -->
  <div id="preloder">
    <div class="loader"></div>
  </div>

  <!-- Navbar -->
  <?php include 'include/navbar.php' ?>
  <!-- Navbar -->

  <!-- Cta Section Begin -->
  <section class="cta-section spad set-bg" data-setbg="img/growth-page-title.jpg">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="cta-text">
            <h2>Financial Report</h2>
            <p>INVESTORS</p>
            <!-- <a href="#" class="primary-btn">Contact us</a> -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Cta Section End -->

  <!-- Testimoial Section Begin -->
  <section class="testimonial-section">
    <div class="container">
      <div class="row">  
        <select onchange="searchFunction()" id="myInput" class="form-select form-control-sm">
          <option value="" selected>All</option>
          <option value="2021">2021</option>
          <option value="2020">2020</option>
          <option value="2019">2019</option>
          <option value="2018">2018</option>
        </select>
        <!-- <input type="text" id="myInput" onkeyup="searchFunction()" placeholder="Search for names.." title="Type in a name"> -->
        <table id="myTable" class="table">
          <tbody>
            <?php foreach($data as $dt) { ?>
            <tr>
              <td style="text-align: center;"><img class="mx-auto" style="width:100px;" src="<?php echo "admin/assets/img/Upload/".$dt['Image']; ?>"></td>
              <td><?php echo $dt['Tahun']; ?> <br />
              <?php echo $dt['Judul']; ?></td>
              <td style="text-align: center;"><a href="<?php echo "admin/assets/pdf/Upload/".$dt['PDF']; ?>" target="_blank">Download</a></td>
              <td hidden><?php echo $dt['Tahun']; ?></td>
            </tr> 
            <?php $no++; } ?>
          </tbody>
        </table>
        </div>
      </div>
    </section>
    <!-- Testimonial Section End -->

    <!-- Footer -->
    <?php include 'include/footer.php' ?>
    <!-- Footer -->
  </body>

  </html>