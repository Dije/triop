<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="img/growth-journey-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>GROWTH JOURNEY</h2>
						<p>ABOUT US</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->

	<ul class="timeline">
        <li>
          <div class="timeline-badge"><img class="rounded-circle" src="assets/img/Growth Journey/growth-journey-1-_resized360x260.jpg" alt="" /></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">2005</h4>
              <p>Our Founding Shareholders went into the business of providing mining contractor services to coal mines in South Kalimantan.</p>
            </div>
            <div class="timeline-body">
              <p>Successive acquisitions of coal mines
				Our Founding Shareholders went into the business of providing mining contractor services to coal mines in South Kalimantan. This led to a series of successive acquisitions of coal mines. Following the growth and development of their coal mining business, our Founding Shareholders decided to branch into coal mining-related businesses, such as coal trading and shipping.</p>
            </div>
          </div>
        </li>
        <li class="timeline-inverted">
          <div class="timeline-badge"><img class="rounded-circle" src="assets/img/Growth Journey/growth-journey-3-_resized360x260.jpg"  alt="" /></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">2008</h4>
              <p>Founding Shareholders ventured into coal trading</p>
            </div>
            <div class="timeline-body">
              <p>Ventured into coal trading
				Founding Shareholders ventured into coal trading with the view to facilitate the sale of coal available in their coal mines.</p>
            </div>
          </div>
        </li>
        <li>
          <div class="timeline-badge"><img class="rounded-circle" src="assets/img/Growth Journey/growth-journey-5-_resized360x260.jpg" alt="" /></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">2011</h4>
              <p>PT DPAL was granted the SIUPAL licence</p>
            </div>
            <div class="timeline-body">
              <p>Granted the SIUPAL licence
				PT DPAL was granted the SIUPAL licence to carry out sea transportation activities within Indonesian territories.</p>
            </div>
          </div>
        </li>
        <li class="timeline-inverted">
          <div class="timeline-badge"><img class="rounded-circle" src="assets/img/Growth Journey/growth-journey-6-_resized360x260.jpg" alt="" /> </div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">2012</h4>
              <p>PT DPAL acquired its first pair of tugboat and bargeg</p>
            </div>
            <div class="timeline-body">
              <p>PT DPAL 
              PT DPAL acquired its first pair of tugboat and barge.</p>
            </div>
          </div>
        </li>
        <li>
          <div class="timeline-badge"><img class="rounded-circle" src="assets/img/Growth Journey/growth-journey-7-_resized360x260.jpg" alt="" /> </div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">2013</h4>
              <p>Our coal trading subsidiary, PT DNS</p>
            </div>
            <div class="timeline-body">
              <p>Focus on the domestic sale of coal
				Established our coal trading subsidiary, PT DNS, to consolidate all coal trading activities in the Group and to focus on the domestic sale of coal.</p> 
            </div>
          </div>
        </li>
        <li class="timeline-inverted">
          <div class="timeline-badge"><img class="rounded-circle" src="assets/img/Growth Journey/growth-journey-12-_resized360x260.jpg" alt="" /> </div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">2014</h4>
              <p>PT DNS was granted the SIUP-M licence</p>
            </div>
            <div class="timeline-body">
              <p>Granted the SIUP-M licence
				PT DNS was granted the SIUP-M licence as distributor, exporter and importer of coal, iron ore, iron sand/manganese.</p>
            </div>
          </div>
        </li>
        <li>
          <div class="timeline-badge"><img class="rounded-circle" src="assets/img/Growth Journey/growth-journey-8-_resized360x260.jpg" alt="" /> </div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">2016</h4>
              <p>PT DPAL was assigned its first long-term coal transportation contract with PT Sinar Deli.</p>
            </div>
            <div class="timeline-body">
              <p>First long-term coal transportation contract
				PT DPAL was assigned its first long-term coal transportation contract with PT Sinar Deli to provide coal transportation services.</p> 
            </div>
          </div>
        </li>
        <li class="timeline-inverted">
          <div class="timeline-badge"><img class="rounded-circle" src="assets/img/Growth Journey/growth-journey-10-_resized360x260.jpg" alt="" /> </div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">2017</h4>
              <p>PT DNS received a three-year IUP-OPK licence to source coal from four coal mines in South Kalimantan.</p>
            </div>
            <div class="timeline-body">
              <p>Three-year IUP-OPK licence
				PT DNS received a three-year IUP-OPK licence to source coal from four coal mines in South Kalimantan. The license is renewable for a maximum of three years for each renewal. We commenced our coal trading business.</p>
            </div>
          </div>
        </li>
        <li>
          <div class="timeline-badge"><img class="rounded-circle" src="assets/img/Growth Journey/growth-journey-11-_resized360x260.jpg" alt="" /> </div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">2019</h4>
              <p>Major achievements</p>
            </div>
            <div class="timeline-body">
              <p>Major Achievements
				Milestone 1
				PT DNS secured fixed term coal sale contracts from customers for aggregate sales of up to 1.5 million metric tonnes (“MT”) of coal, and fixed term coal purchase contracts from suppliers for the aggregate supply of up to 1.9 million MT of coal.

				Milestone 2
				PT DNS was granted a new five-year IUP-OPK licence for the transportation and sale of coal from four coal mines in South Kalimantan. The licence is renewable for a maximum of five years for each renewal.

				Milestone 3
				PT DPAL acquired a 50,000 MT handymax bulk carrier (“Pacific Bulk”) and re-registered it as an Indonesian-flagged vessel in April 2019. Pacific Bulk has a larger transportation capacity and ability to cover longer voyages.

				Milestone 4
			PT DPAL entered into a memorandum of understanding in July 2019 for the charter of Pacific Bulk to transport 35,000 to 50,000 MT of coal per charter for a period of two years.</p> 
            </div>
          </div>
        </li>
    </ul>

	

<!-- Footer -->
<?php include 'include/footer.php' ?>
<!-- Footer -->
</body>

</html>