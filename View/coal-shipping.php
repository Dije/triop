<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="img/Coal-Shipping-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>COAL SHIPPING</h2>
						<p>OUR BUSINESSES</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->

	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row justify-content-center"> 
				<button class="button active" onClick="overviewShippingFunction(this)"><a>Overview</a></button>
				<button class="button" onClick="videosFunction(this)"><a>Videos</a></button>
				<button class="button" onClick="fleetFunction(this)"><a>Fleet</a></button>  
			</div>
		</div>
	</section>
	<div id="OverviewShipping" > 
		<section class="testimonial-section set-bg" data-setbg="img/Subbar-Business-Overview.png">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="section-title">
							<h4>Our policy of continuously repairing and conducting regular maintenance works on our fleet ensures that we operate at optimum efficiency.</h4>
						</div>
					</div>
				</div>
			</section>
			<section class="testimonial-section">
				<div class="container">
					<div class="row">
						<div class="about-text">
							<div class="section-title">
								<p>Through our subsidiary, PT DPAL, we provide chartering services of Indonesian-flagged tugboats, barges and bulk carrier to transport coal within the Indonesian territories. Our shipping routes cover mainly between coal mines located in South Kalimantan and the Java and Sulawesi islands in Indonesia.</p>
							</div>
						</div>
						<div class="section-title">
							<div class="timeline-image"><img class="img-fluid" 
								src="assets/img/Coal Shipping/coal-shipping-1.jpg" alt="" />
							</div>
						</div>
					</div>
				</div>
			</section> 
		</div>

		<div id="Videos" style="display: none;"> 
			<section class="testimonial-section set-bg" data-setbg="img/Subbar-Business-Overview.png">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="section-title">
								<h4>Our 50,000 MT handymax bulk carrier, Pacific Bulk, with its larger transportation capacity and ability to cover longer voyages, functions as a mother vessel, enabling the Group to provide customers with increased shipping capacities, and better control of coal loading and shipment schedules.</h4>
							</div>
						</div>
					</div>
				</section>
				<section class="testimonial-section">
					<div class="container">
						<div class="row">  
							<div style="padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-6 col-sm-6">
								<div class="fs-about">
									<div class="fa-logo2"> 
										<button style="background-image: url('assets/img/Coal Shipping/V1-feat.jpg');" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"></button>
										<div style="margin-left: 20px; margin-right: 20px;">
											Seamless transportation of coal from mine sites to our customers
										</div>
									</div>
								</div>
							</div>
							<div style="padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-6 col-sm-6">
								<div class="fs-about">
									<div class="fa-logo2">
										<button style="background-image: url('assets/img/Coal Shipping/V2-feat.jpg');" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal2"></button>
										<div style="margin-left: 20px; margin-right: 20px;">
											Launch of tugboat Pacific Eleven in Batam, Indonesia on 25 November 2018
										</div>
									</div>
								</div>
							</div>
							<div style="padding-left: 0; padding-right: 0;" class="col-lg-4 col-md-6 col-sm-6">
								<div class="fs-about">
									<div class="fa-logo2">
										<button style="background-image: url('assets/img/Coal Shipping/V3-feat.jpg');" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal3"></button>
										<div style="margin-left: 20px; margin-right: 20px;">
											Refurbishment of the acquired 50,000 MT handymax bulk carrier, Pacific Bulk, in 2019.
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section> 
			</div>

			<div id="Fleet" style="display: none;"> 
				<section class="testimonial-section set-bg" data-setbg="img/Subbar-Business-Overview.png">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="section-title">
									<h4>In accordance with the relevant cabotage laws in Indonesia, sea transportation activities within Indonesia waters can only be done by Indonesian-flagged vessels manned by Indonesian crew.</h4>
								</div>
							</div>
						</div>
					</section>
					<section class="testimonial-section">
						<div class="container">
							<div class="row">
								<div class="col-md-8" style="padding-top: 15px; padding-bottom: 15px;">
									<div><img class="img-fluid" 
										src="assets/img/Our Businesses/coal-trading-map.png" alt="" />
									</div>
								</div>
								<div class="col-md-4" style="display: inline-block; padding-top: 10%; padding-bottom: 10%; text-align: justify;">
									Our Coal Trading Business procures coal from coal mines located in South Kalimantan for domestic sales, while our Coal Shipping Services Business covers domestic shipping routes between coal mines located in South Kalimantan and the Java and Sulawesi islands in Indonesia.
								</div>
							</div>
						</div>
					</section>
				</div>
				<!-- Testimonial Section End -->

				<!-- Footer -->
				<?php include 'include/footer.php' ?>
				<!-- Footer -->
			</body>

			</html>