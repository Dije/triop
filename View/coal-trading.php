<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->
    <?php include 'include/navbar.php' ?>
    <!-- Navbar -->

    <!-- Cta Section Begin -->
    <section class="cta-section spad set-bg" data-setbg="img/trading-risk-management-page-title.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cta-text">
                        <h2>COAL TRADING</h2>
                        <p>OUR BUSINESSES</p>
                        <!-- <a href="#" class="primary-btn">Contact us</a> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Cta Section End -->

    <!-- Testimoial Section Begin -->
    <section class="testimonial-section">
            <div class="container">
                <div class="row justify-content-center">            
                    <button class="button active" onClick="overviewBusinessFunction(this)"><a>Overview</a></button>
                    <button class="button" onClick="TradingRiskManagementFunction(this)"><a>Trading Risk Management</a></button>
                </div>
            </div>
        </section>
    <div  id="OverviewBusiness" >
        <section class="testimonial-section set-bg" data-setbg="img/Subbar-Business-Overview.png">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h4>Our relationships with suppliers allow access to reliable sources of coal at competitive rates.</h4>
                        </div>
                    </div>
                </div>
            </section>
            <section class="testimonial-section">
                <div class="container">
                    <div class="row">
                        <div class="about-text">
                            <div class="section-title"> 
                                <p>We operate our Coal Trading Business through our subsidiary, PT DNS. The thermal coal that we procure are supplied to mainly to coal traders who serve domestic end-users in various industries, including nickel smelting and cement manufacturing.

                                    One of our key strengths is a reliable supply of coal. We have been and will continue procuring coal from third-­party coal mines and coal mines related to our Founding Shareholders.

                                    In financial year 2019, we secured fixed term coal sale contracts from customers for aggregate sales of up to 1.5 million metric tonnes <b> (“MT”) </b> of coal, and fixed term coal purchase contracts from suppliers for the aggregate supply of up to 1.9 million MT of coal.

                                The Group may enter into more fixed term coal sale and/or purchase contracts with our customers and suppliers, depending on our assessment of market conditions and opportunities. </p>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </section>
    </div>
    <section class="testimonial-section">
        <div class="container">
            <div class="section-title"id="TradingRiskmanagement" style="white-space: pre-line; display: none;">
                <div class="row">
                    <div class="col-md-6">
                        <div class="timeline-image"><img class="img-fluid" 
                            src="assets/img/Trading Risk Management/trading-risk-management.jpg" alt="" />
                        </div>
                    </div>
                    <div class="col-md-6" style="display: inline-block; padding-top: 10%; padding-bottom: 10%;">
                        To manage trading risks, we typically source for coal after securing coal purchase contracts with our customers (i.e. the coal sale and coal purchase contracts with our suppliers and customers respectively are entered into on a back-to-back basis). As such, we do not maintain a coal stockpile nor take significant positions on coal prices.
                    </div>
                </div>  
            </div>
        </div>
    </section>
    <!-- Testimonial Section End -->

    <!-- Footer -->
    <?php include 'include/footer.php' ?>
    <!-- Footer -->
</body>

</html>