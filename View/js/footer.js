$(function(){
	var url = window.location;
	// for sidebar menu entirely but not cover treeview
	$('ul.nav a').filter(function() {
		return this.href != url;
	}).parent().removeClass('active');

	// for sidebar menu entirely but not cover treeview
	$('ul.nav a').filter(function() {
		return this.href == url;
	}).parent().addClass('active');

	// for treeview
	$('ul.dropdown a').filter(function() {
		return this.href == url;
	}).parentsUntil(".nav > .dropdown").addClass('active');
})


