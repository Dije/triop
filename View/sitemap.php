<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="img/cta-bg.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>SITEMAP</h2>
						<p>ABOUT TRIOP</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->

	<!-- Footer Section Begin -->
	<footer class="footer-section" style="padding-top: 50px;">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="fs-about">
						<div class="fa-logo">
							<a href="./">
								<img src="img/f-logo.png" alt="">
							</a>
						</div>
						<p>A little brief about PT. TRIOP can be done here.</p>
					<!-- <div class="fa-social">
						<a href="#"><i class="fa fa-facebook"></i></a>
						<a href="#"><i class="fa fa-twitter"></i></a>
						<a href="#"><i class="fa fa-youtube-play"></i></a>
						<a href="#"><i class="fa fa-instagram"></i></a>
					</div> -->
				</div>
				<br />
				<div class="fs-widget">
					<h5>SITEMAP</h5>
					<ul>
						<li><a href="./"><b>Home</b></a></li>
						<br />
						<li><a href="./about-us"><b>About Us</b></a></li>
						<li class="sub"><a href="./about-us">Business Overview</a></li>
						<li class="sub"><a href="./growth-journey">Growth Journey</a></li>
						<li class="sub"><a href="./board-of-directors">Board of Directors</a></li> 
						<li class="sub"><a href="./board-of-commissioners">Board of Commissioners</a></li>                            
					</ul>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="fs-widget">
					<!-- <h5> <br /> </h5> -->
					<ul>
						<li class="sub"><a href="./corporate-structure">Corporate Structure</a></li>
						<li class="sub"><a href="./corporate-organization">Corporate Organization</a></li>
						<br />
						<li><a href="./our-businesses"><b>Our Businesses</b></a></li>
						<li class="sub"><a href="./coal-trading">Coal Trading</a></li>
						<li class="sub"><a href="./coal-shipping">Coal Shipping</a></li>
						<br />
						<li><a href="./sustainability"><b>Sustainability</b></a></li>
						<li class="sub"><a href="./quality-control">Quality Control</a></li>
						<li class="sub"><a href="./health-&-safety">Health & Safety</a></li>
						<li class="sub"><a href="./environmental-matters">Environmental Matters</a></li>
						<li class="sub"><a href="./corporate-social-responsibility">Corporate Social Responsibility</a></li>
						<br />
					</ul>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="fs-widget">
					<ul> 
						<li><a style="cursor: pointer;"><b>Investors</b></a></li>
						<!-- <li class="sub"><a href="./financial-summary">Financials Summary</a></li>
						<li class="sub"><a href="./financial-report">Financials Report</a></li>
						<li class="sub"><a href="./publications">Publications</a></li> -->
						<li class="sub"><a href="./company-report">Company Reports</a></li>
						<li class="sub"><a href="./general-meeting-shareholders">General Meeting Shareholders</a></li>
						<li class="sub"><a href="./stock-information">Stock Information</a></li>
						<li class="sub"><a href="./shareholders-information">Shareholders Information</a></li>
						<li class="sub"><a href="./disclosure-information">Disclosure Information</a></li>
						<!-- <br />
						<li><a href="./newsroom"><b>Newsroom</b></a></li> -->
						<br />
						<li><a href="./contact"><b>Contact Us</b></a></li>
						<br />
						<li><a style="cursor: none;"><b>Language</b></a></li>
						<li class="sub"><a style="padding-left: 3px;" href="id/"><img width="30px" src="./assets/img/indonesia.png">Indonesia</a><li>
							<li class="sub"><a style="padding-left: 3px; cursor: none; color: #333;" class="active"><img width="30px" src="assets/img/english.png">English</a><li> 
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>