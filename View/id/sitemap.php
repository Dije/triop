<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Footer Section Begin -->
	<footer class="footer-section" style="padding-top: 134px;">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="fs-about">
					<div class="fa-logo">
						<a href="./">
							<img src="../img/f-logo.png" alt="">
						</a>
					</div>
					<p>Sedikit pengenalan mengenai PT. TRIOP dapat dilakukan disini.</p>
					<!-- <div class="fa-social">
						<a href="#"><i class="fa fa-facebook"></i></a>
						<a href="#"><i class="fa fa-twitter"></i></a>
						<a href="#"><i class="fa fa-youtube-play"></i></a>
						<a href="#"><i class="fa fa-instagram"></i></a>
					</div> -->
				</div>
				<br />
				<div class="fs-widget">
					<h5>Peta Situs</h5>
					<ul>
						<li><a href="./"><b>Beranda</b></a></li>
						<br />
						<li><a href="./about-us"><b>Tentang Kita</b></a></li>
						<li class="sub"><a href="./about-us">Sekilas Perusahaan</a></li>
						<li class="sub"><a href="./growth-journey">Perjalanan Perusahaan</a></li>
						<li class="sub"><a href="./board-of-directors">Dewan Direksi</a></li> 
						<li class="sub"><a href="./board-of-commissioners">Dewan Komisaris</a></li>                               
					</ul>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="fs-widget">
					<!-- <h5> <br /> </h5> -->
					<ul>
						<li class="sub"><a href="./corporate-structure">Struktur Perusahaan</a></li>
						<li class="sub"><a href="./corporate-organization">Organisasi Perusahaan</a></li> 
						<br />
						<li><a href="./our-businesses"><b>Penambangan</b></a></li>
						<li class="sub"><a href="./coal-trading">Perdaganan Batu Bara</a></li>
						<li class="sub"><a href="./coal-shipping">Pengiriman Batu Bara</a></li>
						<br />
						<li><a href="./sustainability"><b>Keberlangsungan</b></a></li>
						<li class="sub"><a href="./quality-control">Kontrol Kualitas</a></li>
						<li class="sub"><a href="./health-&-safety">Kesehatan & keamanan</a></li>
						<li class="sub"><a href="./environmental-matters">Masalah Lingkungan</a></li>
						<li class="sub"><a href="./corporate-social-responsibility">Tanggung Jawab Sosial Perusahaan</a></li>
						<br />
					</ul>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6">
				<div class="fs-widget">
					<ul> 
						<li><a style="cursor: pointer;"><b>Investor</b></a></li>
						<!-- <li class="sub"><a href="./financial-summary">Ringkasan Keuangan</a></li>
						<li class="sub"><a href="./financial-report">Laporan Keuangan</a></li>
						<li class="sub"><a href="./publications">Publikasi</a></li> -->
						<li class="sub"><a href="./#">Informasi Keuangan</a></li>
                        <li class="sub"><a href="./#">RUPS</a></li>
                        <li class="sub"><a href="./#">Informasi Saham</a></li>
                        <li class="sub"><a href="./#">Kepemilikan Saham</a></li>
                        <li class="sub"><a href="./#">Keterbukaan Informasi</a></li>
						<br />
						<!-- <li><a href="./newsroom"><b>Berita</b></a></li>
						<br /> -->
						<li><a href="./contact"><b>Hubungi Kami</b></a></li>
						<br />
 						<li><a style="cursor: none;"><b>Bahasa</b></a></li>
                        <li class="sub"><a style="padding-left: 3px; cursor: none; color: #333;"><img width="30px" src="../assets/img/indonesia.png">Indonesia</a><li> 
                        <li class="sub"><a style="padding-left: 3px;" href="../"><img width="30px" src="../assets/img/english.png">English</a><li>
					</ul>
				</div>
			</div>
		</footer>

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer --> 
	</body>

	</html>