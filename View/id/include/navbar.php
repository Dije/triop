<script async src="https://cse.google.com/cse.js?cx=caf30d28032f80ffc"></script>
<header class="header-section">
    <div style="background-color: #f1f1f1;transition: 0.4s; position: fixed; width: 100%; top: 0; z-index: 99;" class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="logo">
                    <a href="./">
                        <img src="../img/logo.png" alt="">
                    </a>
                </div>
                <nav class="nav-menu mobile-menu">
                    <ul class="nav">
                        <li class="active"><a href="./">Beranda</a></li>
                        <li><a href="./about-us">Tentang Kita</a>
                            <ul class="dropdown" role="menu">
                                <li><a href="./about-us">Sekilas Perusahaan</a></li>
                                <li><a href="./growth-journey">Perjalanan Perusahaan</a></li>
                                <li><a href="./board-of-directors">Dewan Direksi</a></li>
                                <li><a href="./board-of-commissioners">Dewan Komisaris</a></li>
                                <li><a href="./corporate-structure">Struktur Perusahaan</a></li>
                                <li><a href="./corporate-organization">Organisasi Perusahaan</a></li>
                            </ul>
                        </li>
                        <li><a href="./our-businesses">Penambangan</a>
                            <ul class="dropdown">
                                <li><a href="./coal-trading">Perdaganan Batu Bara</a></li>
                                <li><a href="./coal-shipping">Pengiriman Batu Bara</a></li>
                            </ul>
                        </li>
                        <li><a href="./sustainability">Keberlangsungan</a>
                            <ul class="dropdown">
                                <li><a href="./quality-control">Kontrol Kualitas</a></li>
                                <li><a href="./health-&-safety">Kesehatan & keamanan</a></li>
                                <li><a href="./environmental-matters">Masalah Lingkungan</a></li>
                                <li><a href="./corporate-social-responsibility">Tanggung Jawab Sosial Perusahaan</a></li>
                            </ul>
                        </li>
                        <li><a style="cursor: pointer;">Investor</a>
                            <ul class="dropdown">
                                <!-- <li><a href="./financial-summary">Ringkasan Keuangan</a></li> -->
                                <!-- <li><a href="./financial-report">Laporan Keuangan</a></li>
                                    <li><a href="./publications">Publikasi</a></li> -->
                                    <li><a href="./#">Informasi Keuangan</a></li>
                                    <li><a href="./#">RUPS</a></li>
                                    <li><a href="./#">Informasi Saham</a></li>
                                    <li><a href="./#">Kepemilikan Saham</a></li>
                                    <li><a href="./#">Keterbukaan Informasi</a></li>
                                </ul>
                            </li>
                            <!-- <li><a href="./newsroom">Berita</a></li> -->
                            <li><a href="./contact">Hubungi Kami</a></li>
                            <li class="lang"> 
                                <a href="id/" style="cursor: none;color: #B3E5FC;border: 2px solid #111;border-top-left-radius: 20px;border-bottom-left-radius: 20px;height: 0px;padding-bottom: 24px;background: #111;opacity: 0.5;">ID</a>  
                                <a href="../" style="cursor: pointer;">EN</a>
                            </li>  
                            <div class="gcse-search"></div> 
                        </ul>
                    </nav>
                    <div id="mobile-menu-wrap"></div>
                </div>
            </div>
        </div>
    </header>