<!-- Modal -->
<?php include 'modal.php' ?>
<!-- Modal -->

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

<!-- Footer Section Begin -->
<footer class="footer-section"> 
	<div class="col-lg-12">
		<div class="copyright-text">
			<p> Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved. <a href="sitemap">Sitemap</a></p> 
		</div>
	</div>
</footer>
<!-- Footer Section End -->

<!-- Script -->
<?php include 'script.php' ?>
<!-- Script -->