<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Hero Section Begin -->
	<section class="hero-section">
		<div class="hs-slider owl-carousel">
			<div class="hs-item set-bg" data-setbg="../img/cta-bg.jpg">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="hs-text">
								<!-- <h2>Photography Studio</h2> -->
								<p>Memiliki 9 kapal berbendera Indonesia. Perkiraan kapasitas armada 116.000 MT, tingkat pemanfaatan armada sebesar 100%</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="hs-item set-bg" data-setbg="../img/growth-journey-page-title.jpg">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="hs-text">
								<!-- <h2>Photography Studio</h2> -->
								<p>Menunggangi peluang dalam meningkatnya permintaan batubara Indonesia dan pengiriman batubara domestik</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Hero Section End -->

	<!-- Footer -->
	<?php include 'include/footer.php' ?>
	<!-- Footer --> 
</body>

</html>