<script async src="https://cse.google.com/cse.js?cx=caf30d28032f80ffc"></script>
<header class="header-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="logo">
                    <a href="./">
                        <img src="img/logo.png" alt="">
                    </a>
                </div>
                <nav class="nav-menu mobile-menu">
                    <ul class="nav">
                        <li class="active"><a href="./">Home</a></li>
                        <li><a href="./about-us">About Us</a>
                            <ul class="dropdown" role="menu">
                                <li><a href="./about-us">Business Overview</a></li>
                                <li><a href="./growth-journey">Growth Journey</a></li>
                                <li><a href="./board-of-directors">Board of Directors</a></li>
                                <li><a href="./board-of-commissioners">Board of Commissioners</a></li>
                                <li><a href="./corporate-structure">Corporate Structure</a></li>
                                <li><a href="./corporate-organization">Corporate Organization</a></li>
                            </ul>
                        </li>
                        <li><a href="./our-businesses">Our Businesses</a>
                            <ul class="dropdown">
                                <li><a href="./coal-trading">Coal Trading</a></li>
                                <li><a href="./coal-shipping">Coal Shipping</a></li>
                            </ul>
                        </li>
                        <li><a href="./sustainability">Sustainability</a>
                            <ul class="dropdown">
                                <li><a href="./quality-control">Quality Control</a></li>
                                <li><a href="./health-&-safety">Health & Safety</a></li>
                                <li><a href="./environmental-matters">Environmental Matters</a></li>
                                <li><a href="./corporate-social-responsibility">Corporate Social Responsibility</a></li>
                            </ul>
                        </li>
                        <li><a style="cursor: pointer;">Investors</a>
                            <ul class="dropdown">
                                <!-- <li><a href="./financial-summary">Financials Summary</a></li> -->
                                <!-- <li><a href="./financial-report">Financials Report</a></li>
                                <li><a href="./publications">Publications</a></li> -->
                                <li><a href="./company-report">Company Reports</a></li>
                                <li><a href="./general-meeting-shareholders">General Meeting Shareholders</a></li>
                                <li><a href="./stock-information">Stock Information</a></li>
                                <li><a href="./shareholders-information">Shareholders Information</a></li>
                                <li><a href="./disclosure-information">Disclosure Information</a></li>
                            </ul>
                        </li>
                        <!-- <li><a href="./newsroom">Newsroom</a></li> -->
                        <li><a href="./contact">Contact Us</a></li>
                        <li class="lang">
                            <a href="id/" style="cursor: pointer; padding-right: 0;">ID</a>  
                            <a style="cursor: none;color: #B3E5FC;border: 2px solid #111;border-top-right-radius: 20px;border-bottom-right-radius: 20px;height: 0px;padding-bottom: 24px;background: #111;opacity: 0.5;">EN</a>
                        </li>  
                        <div class="gcse-search"></div> 
                    </ul>
                </nav>
                <div id="mobile-menu-wrap"></div>
            </div>
        </div>
    </div>
</header>