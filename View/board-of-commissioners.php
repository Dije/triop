<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->
  
<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->
    <?php include 'include/navbar.php' ?>
    <!-- Navbar -->

    <!-- Cta Section Begin -->
    <section class="cta-section spad set-bg" data-setbg="img/bod-page-title.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cta-text">
                        <h2>BOARD OF COMMISSIONERS</h2>
                        <p>ABOUT US</p>
                        <!-- <a href="#" class="primary-btn">Contact us</a> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Cta Section End -->

    <!-- Testimoial Section Begin -->
    <section class="testimonial-section">
        <div class="container">
            <div class="row flex-column flex-md-row">  
                <img src="assets/img/Board of Directors/RGD-bod.jpg" class="center"> 
                <span class="dot" data-toggle="popover" data-placement="bottom"></span>
            </div>
        </div>
    </section>
    <!-- Testimonial Section End -->
    
    <div class="container"> 
    </div>
        <!-- Footer -->
        <?php include 'include/footer.php' ?>
        <!-- Footer -->
    </body>

    </html>

