<?php 
define('PROJECT_ROOT_PATH', __DIR__);
session_start();

include_once (PROJECT_ROOT_PATH . '/../../Controller/laporanController.php');
include_once (PROJECT_ROOT_PATH . '/../../Controller/userController.php');
$laporan = new laporanController();
$user = new userController();

$data = $laporan->getData();
$no = 1;

if ($user->get_session() == true) {
  if (isset($_POST['submit'])){
    $ekstensi_diperbolehkan = array('png','jpg');
    $nama = $_FILES['file']['name'];
    $x = explode('.', $nama);
    $ekstensi = strtolower(end($x));
    $ukuran = $_FILES['file']['size'];
    $file_tmp = $_FILES['file']['tmp_name'];

    $ekstensi_diperbolehkan2 = array('pdf');
    $nama2 = $_FILES['file2']['name'];
    $y = explode('.', $nama2);
    $ekstensi2 = strtolower(end($y));
    $ukuran2 = $_FILES['file2']['size'];
    $file_tmp2 = $_FILES['file2']['tmp_name'];

    if(in_array($ekstensi, $ekstensi_diperbolehkan) === true && in_array($ekstensi2, $ekstensi_diperbolehkan2) === true){
      if($ukuran < 1044070){      
        move_uploaded_file($file_tmp, 'assets/img/Upload/'.$nama);
        move_uploaded_file($file_tmp2, 'assets/pdf/Upload/'.$nama2);
            chmod('assets/img/Upload/'.$nama, 0664);
            chmod('assets/pdf/Upload/'.$nama2, 0664);
        $add = $laporan->addReport($_POST['judul'], $_POST['tahun'], $nama, $_POST['desc'], $nama2);
        if($add){
          echo "<script type='text/javascript'>alert('Report Added Success');</script>";
        }else{
          echo "<script type='text/javascript'>alert('Report Added Failed');</script>";
        }
      }else{
        echo "<script type='text/javascript'>alert('File Too Big');</script>";
      }
    }else{
      echo "<script type='text/javascript'>alert('Extension Is Not Allowed');</script>";
    }
    echo "<script type='text/javascript'>window.location='report.php'</script>";
  }
}else{
  header("location:index.php");
}
?>

<!-- SideBar -->
<?php include 'include/header.php' ?>
<!-- SideBar -->

<body class="">
  <div class="wrapper ">
    <!-- SideBar -->
    <?php include 'include/sidebar.php' ?>
    <!-- SideBar -->

    <div class="main-panel">
      <!-- NavBar -->
      <?php include 'include/navbar.php' ?>
      <!-- NavBar -->

      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header ">
                <h5 class="card-title">ADD REPORT</h5>
                <!-- <p class="card-category">24 Hours performance</p> -->
              </div>
              <div class="card-body ">
                <form action="" method="post" class="user" enctype="multipart/form-data">
                  <div class="form-group">
                    <input type="text" class="form-control form-control-user" name="judul" id="exampleJudul" placeholder="Title">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control form-control-user" name="tahun" id="exampleTahun" placeholder="Year">
                  </div>
                  <label for="file">Select a Image:</label>
                  <input type="file" id="file" name="file"><br><br>
                  <label for="file2">Select a Report:</label>
                  <input type="file" id="file2" name="file2"><br><br>
                  <div class="form-group">
                    <input type="text" class="form-control form-control-user" name="desc" id="exampleDesc" placeholder="Description">
                  </div>
                </div>
                <hr>
                <div class="card-footer ">
                  <div class="stats">
                   <button name="submit" class="btn btn-primary btn-user btn-block"> Add Report </button>
                 </div>
               </form>
             </div>
           </div>
         </div>
       </div>
       <div class="row">
        <div class="col-md-12">
          <div class="card ">
            <div class="card-header ">
              <h5 class="card-title">REPORT LIST</h5>
              <!-- <p class="card-category">24 Hours performance</p> -->
            </div>
            <div class="card-body ">
              <div class="row">
                <div class="table-responsive">  
                  <table id="myTable" class="table table-hover table-dark">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Tahun</th>
                        <th scope="col">Image</th>
                        <th scope="col">Desc</th>
                        <th scope="col">PDF</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>

                    <tbody>
                      <?php 
                      if($data=="-"){ ?>
                        <tr><td>-</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td>
                          <td>-</td></tr>
                        <?php }else{
                          foreach($data as $dt) { ?>
                            <tr id="header">
                              <th scope="row"><?php echo $no; ?></th>
                              <td><?php echo $dt['Judul']; ?></td>
                              <td><?php echo $dt['Tahun']; ?></td>
                              <td><?php echo $dt['Image']; ?></td>
                              <td><?php echo $dt['Des']; ?></td>
                              <td><?php echo $dt['PDF']; ?></td>
                              <td>
                                <a href="#?id=<?php echo $dt['ID_Laporan']; ?>" class="btn btn-info btn"> Edit</a>
                                <a href="#?id=<?php echo $dt['ID_Laporan']; ?>" class="btn btn-danger btn"> Delete</a>
                              </td>
                            </tr>
                            <?php $no++; }
                          }?>
                        </tbody>
                      </table>
                    </div>
                    <div class="col-md-12 text-center">
                      <ul class="pagination pagination-lg pager" id="myPager"></ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <?php include 'include/footer.php' ?>
      <!-- Footer -->
    </div>
  </div>
  <!-- Script -->
  <?php include 'include/script.php' ?>
  <!-- Script -->
</body>

</html>
