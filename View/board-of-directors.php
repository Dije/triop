<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="img/management-team-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>BOARD OF DIRECTORS</h2>
						<p>ABOUT US</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->

	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row">  
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="fs-about">
						<div class="fa-logo1">
							<img src="assets/img/Board of Directors/Lee-Yaw-Loong-Francis-1.jpg" alt="" >
							<div class="content"> 
								<p>EXECUTIVE DIRECTOR AND CHIEF EXECUTIVE OFFICER</p>
								<div class="fa-name row">
									<div class="col-sm-10">
										<h2>Francis Lee</h2>
									</div>
									<div class="col-sm-2">
										<h2 id="click_advance"><i onClick="myFunction()" class="fa fa-angle-double-down"></i></h2>
									</div>
								</div> 
							</div> 
							<div id="destination">
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="fs-about">
						<div class="fa-logo1">
							<img src="assets/img/Board of Directors/Salim-Limanto-1.jpg" alt="" >
							<div class="content"> 
								<p>EXECUTIVE DIRECTOR AND CHIEF OPERATING OFFICER</p>
								<div class="fa-name row">
									<div class="col-sm-10">
										<h2>Salim Limanto</h2>
									</div>
									<div class="col-sm-2">
										<h2 id="click_advance2"><i onClick="myFunction2()" class="fa fa-angle-double-down"></i></h2>
									</div>
								</div>                                
							</div> 
							<div id="destination2">
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="fs-about">
						<div class="fa-logo1">
							<img src="assets/img/Board of Directors/Thomas-Yeo-Tze-Khern.jpg" alt="" >
							<div class="content" > 
								<p>CHIEF FINANCIAL OFFICER </p>     
								<div class="fa-name row">
									<div class="col-sm-10">
										<h2>Yeo Tze Khern </h2>
									</div>
									<div class="col-sm-2">
										<h2 id="click_advance3"><i onClick="myFunction3()" class="fa fa-angle-double-down"></i></h2>
									</div>
								</div>
							</div>
							<div id="destination3">
							</div>
						</div>
					</div>
				</div>
			</div> 
			<div class="container"> 
				<div class="about-text">
					<div class="section-title" id="myDIV" style="display: none;"> 
						<p> Mr Francis Lee is responsible for the overall management, as well as the strategic planning and development of the Group’s business, spearheading the expansion and growth of the Group. Mr Lee has over 25 years of experience and expertise in managing companies in the trading, shipping, investment holding and agriculture sectors.

						Mr Lee started his career as an auditor in Coopers & Lybrand Singapore, now known as PricewaterhouseCoopers, from 1991 to 1995. From 1995 to 1997, he was the General Manager of Coopers & Lybrand Hla Tun Consultants in Yangon, Myanmar. From 1997 to 1998, Mr Lee joined Kuok (Singapore) Ltd. as the Assistant General Manager in Myanmar. Subsequently from 1998 to 2000, he was transferred to Pacific Carrier Ltd, a subsidiary of Kuok (Singapore) Ltd, where he acted as Group Financial Controller. Pacific Carrier Ltd was listed on the SGX-ST from 1990 to 2001. From 2001 to 2003, Mr Lee was the Group Financial Controller of Kuok (Singapore) Ltd. From 2004 to 2015, Mr Lee was appointed the General Manager of the fertilizer department at NewQuest (Trading) Pte Ltd (now known as Agrifert Holdings Pte. Ltd.), a subsidiary of Kuok (Singapore) Ltd. As part of his various appointments in the Kuok group of companies, Mr Lee has also held various positions, such as chairman of NewQuest Vietnam Company Ltd (now known as Agrifert Vietnam Ltd), a Vietnamese subsidiary of Agrifert Holdings Pte. Ltd. from 2011 to 2019, as General Manager of KSM Strategic Pte Ltd, a subsidiary in the Kuok group of companies from 2014 to 2015, and in Agri Malar Company Limited (Myanmar). From 2016 to 2019, he served as the General Manager and Director of Agrifert Trading Pte. Ltd., immediately prior to joining our Group as our Executive Director and Chief Executive Officer.

						Mr Lee has also previously sat on the board of Beng Kuang Marine Ltd, a company listed on the Mainboard of the Singapore Exchange, as an Alternate Non-Executive Director from 2013 to 2016.

						Mr Lee graduated from Monash University, Melbourne, Australia with a Bachelor of Economics (Honours), majoring in accounting and computer science in 1992. He is a member of the Australian Society of Certified Practising Accountants (now known as “CPA Australia”). </p>
					</div>  
				</div>
			</div>
			<div class="container">  
				<div class="about-text">
					<div class="section-title" id="myDIV2" style="display: none;"> 
						<p>Mr Salim Limanto is responsible for the overall operations and business development activities of our Group. Mr Limanto has over 11 years of management and business development experience in the coal mining, transportation and trading industries, and has been involved in the Group’s business since the inception of PT DNS and PT DPAL in 2013 and 2010, respectively.

							Mr Limanto started his career in PT Sinar Deli, which was previously one of the domestic coal trading entities of the Deli Coal Group, where he was Head of Sales and Shipping from 2006 to 2018. He holds directorships in several companies such as PT DNS and PT DPAL.

						Mr Limanto obtained a Bachelor Degree from the Universitas Tarumanagara, Jakarta, Indonesia in 2006. He is the eldest son of Mr Djunaidi Hardi and a nephew of Mr Juhadi, Mr Arifin Ang and Mr Limas Ananto, all of whom are Founding Shareholders of our Company.</p>
					</div>  
				</div>
			</div>
			<div class="container">  
				<div class="about-text">
					<div class="section-title" id="myDIV3" style="display: none;"> 
						<p>Mr Yeo Tze Khern is responsible for the accounting and financial functions of the Group.

							Mr Yeo started his career as an Auditor in Ernst & Young (Singapore) from 1999 to 2002. From 2002 to 2005, he was an Audit Manager in Ernst & Young Hua Ming (Beijing, China). Subsequently Mr Yeo joined Lehmanbrown International Accounting (Shanghai, China) as a Senior Manager from 2005 to 2007. From 2007 to 2009, he was a Director at PKF International Accounting (Shanghai, China). From 2009 to 2018, Mr Yeo acted as the Chief Financial Officer and Company Secretary of China Mining International Limited, a company listed on the Main Board of SGX-ST. In 2018, Mr Yeo joined RID as the Chief Financial officer before he was subsequently transferred to the Company.

						Mr Yeo graduated with a Bachelor of Business (Marketing) from Monash University, Australia in 1997 and obtained a Master of Practising Accounting from Monash University, Australia in 1999. He is a Chartered Accountant and a member of the Institute of Singapore Chartered Accountants, and fellows of CPA Australia and the Hong Kong Institute of Certified Public Accountants. Mr Yeo is also a member of the Singapore Institute of Directors.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

	<!-- Footer -->
	<?php include 'include/footer.php' ?>
	<!-- Footer -->
</body>

</html>