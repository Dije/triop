<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Navbar -->
    <?php include 'include/navbar.php' ?>
    <!-- Navbar -->

    <!-- Cta Section Begin -->
    <section class="cta-section spad set-bg" data-setbg="img/environmental-matters-page-title.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cta-text">
                        <h2>ENVIRONMENTAL MATTERS</h2>
                        <p>SUSTAINABILITY</p>
                        <!-- <a href="#" class="primary-btn">Contact us</a> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Cta Section End -->
    
    <!-- Testimoial Section Begin -->
    <section class="testimonial-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12"> 
                    <div class="about-text">
                        <div class="section-title"> 
                            <p> We conduct all aspects of our business in compliance with the applicable environmental laws, and have instituted training sessions, operational policies and directives to (i) ensure that our employees and vessel crew observe all applicable environmental laws and regulations; and (ii) prevent, mitigate and manage the occurrence of environmental pollution originating from our vessels.

                            We have also extended the same environmental and quality requirements to the shipyards we engage for the construction of our new vessels and barges and the ongoing maintenance of our existing fleet. Such shipyards are assessed and selected based on, inter alia, their compliance track record in respect of environmental laws and regulations, and their adopted initiatives on environmental protection and sustainability practices.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Testimonial Section End -->

<!-- Footer -->
<?php include 'include/footer.php' ?>
<!-- Footer -->
</body>

</html>