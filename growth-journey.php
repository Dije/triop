<!-- Header -->
<?php include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Cta Section Begin -->
	<section class="cta-section spad set-bg" data-setbg="img/growth-journey-page-title.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="cta-text">
						<h2>GROWTH JOURNEY</h2>
						<p>ABOUT US</p>
						<!-- <a href="#" class="primary-btn">Contact us</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Cta Section End -->


	<!-- Categories Section Begin -->
	<div class="uk-margin"> </div>
	<div class="uk-section"><div class="owl-carousel owl-theme without-loop">
		<div class="item">
			<div class="uk-card uk-card-primary uk-card-hover uk-card-body uk-light">
				<img src="assets/img/Growth Journey/growth-journey-1-_resized360x260.jpg" alt="" />
				<h3 class="uk-card-title">2005</h3>
				<p>Our Founding Shareholders went into the business of providing mining contractor services to coal mines in South Kalimantan.</p>
				<a href="./about-us"> <button class="primary-btn">Read More</button></a>
				<!-- <h3>$000,000,000.00</h3> -->
			<!-- Successive acquisitions of coal mines
				Our Founding Shareholders went into the business of providing mining contractor services to coal mines in South Kalimantan. This led to a series of successive acquisitions of coal mines. Following the growth and development of their coal mining business, our Founding Shareholders decided to branch into coal mining-related businesses, such as coal trading and shipping. -->
			</div>
		</div>
		<div class="item">
			<div class="uk-card uk-card-primary uk-card-hover uk-card-body uk-light">
				<img src="assets/img/Growth Journey/growth-journey-3-_resized360x260.jpg"  alt="" /> 
				<h3 class="uk-card-title">2008</h3>
				<p>Founding Shareholders ventured into coal trading</p>
				<a href="./about-us"> <button class="primary-btn">Read More</button></a>
				<!-- <h3>$000,000,000.00</h3> -->
				<!-- Ventured into coal trading
				Founding Shareholders ventured into coal trading with the view to facilitate the sale of coal available in their coal mines. -->
			</div>
			
		</div>
		<div class="item">
			<div class="uk-card uk-card-primary uk-card-hover uk-card-body uk-light">
				<img src="assets/img/Growth Journey/growth-journey-5-_resized360x260.jpg" alt="" /> 
				<h3 class="uk-card-title">2011</h3>
				<p>PT DPAL was granted the SIUPAL licence</p>
				<a href="./about-us"> <button class="primary-btn">Read More</button></a>
				<!-- Granted the SIUPAL licence
							PT DPAL was granted the SIUPAL licence to carry out sea transportation activities within Indonesian territories. -->
			</div>
		</div>
		<div class="item">
			<div class="uk-card uk-card-primary uk-card-hover uk-card-body uk-light">
				<img src="assets/img/Growth Journey/growth-journey-6-_resized360x260.jpg" alt="" /> 
				<h3 class="uk-card-title">2012</h3>
				<p>PT DPAL acquired its first pair of tugboat and barge</p>
				<a href="./about-us"> <button class="primary-btn">Read More</button></a>
				<!-- PT DPAL acquired its first pair of tugboat and barge. -->
			</div>

		</div>
		<div class="item">
			<div class="uk-card uk-card-primary uk-card-hover uk-card-body uk-light">
				<img src="assets/img/Growth Journey/growth-journey-7-_resized360x260.jpg" alt="" /> 
				<h3 class="uk-card-title">2013</h3>
				<p>Our coal trading subsidiary, PT DNS</p>
				<a href="./about-us"> <button class="primary-btn">Read More</button></a>
				<!-- Focus on the domestic sale of coal
							Established our coal trading subsidiary, PT DNS, to consolidate all coal trading activities in the Group and to focus on the domestic sale of coal. -->
			</div>
			
		</div>
		<div class="item">
			<div class="uk-card uk-card-primary uk-card-hover uk-card-body uk-light">
				<img src="assets/img/Growth Journey/growth-journey-12-_resized360x260.jpg" alt="" /> 
				<h3 class="uk-card-title">2014</h3>
				<p>PT DNS was granted the SIUP-M licence</p>
				<a href="./about-us"> <button class="primary-btn">Read More</button></a>
				<!-- Granted the SIUP-M licence
							PT DNS was granted the SIUP-M licence as distributor, exporter and importer of coal, iron ore, iron sand/manganese. -->
			</div>
			
		</div>
		<div class="item">
			<div class="uk-card uk-card-primary uk-card-hover uk-card-body uk-light">
				<img src="assets/img/Growth Journey/growth-journey-8-_resized360x260.jpg"  alt="" /> 
				<h3 class="uk-card-title">2016</h3>
				<p>PT DPAL was assigned its first long-term coal transportation contract with PT Sinar Deli.</p>
				<a href="./about-us"> <button class="primary-btn">Read More</button></a>
				<!-- First long-term coal transportation contract
							PT DPAL was assigned its first long-term coal transportation contract with PT Sinar Deli to provide coal transportation services. -->
			</div>
			
		</div>
		<div class="item">
			<div class="uk-card uk-card-primary uk-card-hover uk-card-body uk-light">
				<img src="assets/img/Growth Journey/growth-journey-10-_resized360x260.jpg" alt="" /> 
				<h3 class="uk-card-title">2017</h3>
				<p>PT DNS received a three-year IUP-OPK licence to source coal from four coal mines in South Kalimantan.</p>
				<a href="./about-us"> <button class="primary-btn">Read More</button></a>
				<!-- Three-year IUP-OPK licence
							PT DNS received a three-year IUP-OPK licence to source coal from four coal mines in South Kalimantan. The license is renewable for a maximum of three years for each renewal. We commenced our coal trading business. -->
			</div>
			
		</div>
		<div class="item">
			<div class="uk-card uk-card-primary uk-card-hover uk-card-body uk-light">
				<img src="assets/img/Growth Journey/growth-journey-11-_resized360x260.jpg" alt="" /> 
				<h3 class="uk-card-title">2019</h3>
				<p>Major achievements</p>
				<a href="./about-us"> <button class="primary-btn">Read More</button></a>
				<!-- Major Achievements
								Milestone 1
								PT DNS secured fixed term coal sale contracts from customers for aggregate sales of up to 1.5 million metric tonnes (“MT”) of coal, and fixed term coal purchase contracts from suppliers for the aggregate supply of up to 1.9 million MT of coal.

								Milestone 2
								PT DNS was granted a new five-year IUP-OPK licence for the transportation and sale of coal from four coal mines in South Kalimantan. The licence is renewable for a maximum of five years for each renewal.

								Milestone 3
								PT DPAL acquired a 50,000 MT handymax bulk carrier (“Pacific Bulk”) and re-registered it as an Indonesian-flagged vessel in April 2019. Pacific Bulk has a larger transportation capacity and ability to cover longer voyages.

								Milestone 4
							PT DPAL entered into a memorandum of understanding in July 2019 for the charter of Pacific Bulk to transport 35,000 to 50,000 MT of coal per charter for a period of two years.! -->
			</div>

		</div>
	</div>
</div>

<!-- Categories Section End -->

<!-- Footer -->
<?php include 'include/footer.php' ?>
<!-- Footer -->
</body>

</html>