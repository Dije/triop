<?php
class userController{
	protected $conn;

	public function __construct(){
		$this->conn = mysqli_connect("192.168.100.88", "deli", "Deli123", "website", "3306"); //(host, username, password, database, port)
	}

	public function getData(){
		$query = mysqli_query($this->conn,"SELECT * FROM user");
		$jumdata= mysqli_num_rows($query);
		if($jumdata==0){
			$data="-";
		} else{
			while($row = mysqli_fetch_array($query)){
				$data[]=$row;
			}
		}
		return $data;
	}

	public function check_login($username, $password){
		$query = "SELECT * FROM user WHERE username='$username' and password='$password'";

		  //checking if the username is available in the table
		$result = mysqli_query($this->conn,$query);
		$user_data = mysqli_fetch_array($result);
		$count_row = $result->num_rows;

		if ($count_row == 1) {
			   // this login var will use for the session thing
			$_SESSION['login'] = true;
			$_SESSION['uid'] = $user_data['id_user'];
			$_SESSION['Role'] = $user_data['Role'];
			$_SESSION['FirstName'] = $user_data['firstname'];
			return true;
		}
		else{
			return false;
		}
	}

	public function get_session(){
		return $_SESSION['login'];
	}

	public function get_Role(){
		return $_SESSION['Role'];
	}

	public function get_FirstName(){
		return $_SESSION['FirstName'];
	}

	public function user_logout() {
		$_SESSION['login'] = FALSE;
		session_destroy();
	}
}
?>